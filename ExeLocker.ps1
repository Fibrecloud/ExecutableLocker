# General Notes
# • $env:P2EIncFilePath
# • [Enum]::GetValues([System.ConsoleColor]) | ForEach-Object {Write-Host $_ -ForegroundColor $_}

# Console Title
$Host.UI.RawUI.WindowTitle = "Executable Locker (Windows PowerShell)"
$Host.UI.RawUI.ForegroundColor = "White"
$Host.UI.RawUI.BackgroundColor = "Black"

# Change Console Position and Size
Add-Type -AssemblyName "System.Windows.Forms"
Add-Type -Name "Window" -Namespace "Console" -MemberDefinition '[DllImport("Kernel32.dll")] public static extern IntPtr GetConsoleWindow();[DllImport("user32.dll")] public static extern bool MoveWindow(IntPtr hWnd, int X, int Y, int W, int H);'
[Console.Window]::MoveWindow([Console.Window]::GetConsoleWindow(), ([System.Windows.Forms.SystemInformation]::PrimaryMonitorSize.Width / 2) - (800 / 2), ([System.Windows.Forms.SystemInformation]::PrimaryMonitorSize.Height / 2) - (520 / 2), 800, 520)

# Update Buffer to Match Size
$Host.UI.RawUI.WindowSize = New-Object System.Management.Automation.Host.Size($Host.UI.RawUI.WindowSize.Width, $Host.UI.RawUI.WindowSize.Height)
$Host.UI.RawUI.BufferSize = New-Object System.Management.Automation.Host.Size($Host.UI.RawUI.WindowSize.Width, $Host.UI.RawUI.WindowSize.Height)

# Define Error Verbose
$ErrorActionPreference = "SilentlyContinue"

# Character Display Correction
$OutputEncoding = [Console]::InputEncoding = [Console]::OutputEncoding = New-Object -TypeName "System.Text.UTF8Encoding"

# Console Screen Refresh
[System.Console]::Clear()

# Setup Initalisation Module
function Initalisation() {

    # Show Message on Missing Password File
    if (!(([System.IO.FileInfo]$ExecutionContext.SessionState.Path.GetUnresolvedProviderPathFromPSPath($ExecutionContext.InvokeCommand.ExpandString((Get-Location | Select-Object -ExpandProperty "Path") + "\" + "Password"))).Exists)) {

        # Console Information
        Write-Host "Welcome to Executable Locker"
        Write-Host "You can set a password to prevent unauthorized access to"
        Write-Host "executables and data. Please ensure that the configuration"
        Write-Host "file has been setup so that no errors occur during execution."

        # Console Seperation
        Write-Host ""

    }

    # Console Title
    $Host.UI.RawUI.WindowTitle = "Executable Locker (Initalisation)"

    # Environment Directory
    $Global:ProcessRoute = Get-Location | Select-Object -ExpandProperty "Path"

    # Environment GUID
    $Global:ProcessGUID = (([System.Guid]::NewGuid()).Guid).ToUpper()

    # Console Information
    Write-Host "Terminal Log"
    Write-Host "• Loading Configuration Module"

    # Locate Configuration File
    :Search foreach ($File in (Get-ChildItem -Path $ProcessRoute -Filter "*.json" -Recurse)) {

        # Locate Identification String
        if (([System.IO.File]::ReadAllText($File.FullName)).Contains("0x46-0x69-0x62-0x72-0x65-0x73-0x69-0x74-0x65")) {

            # Load JavaScript Object Notation File
            $Global:FileData = Get-Content -Path $File.FullName -Raw -Encoding "UTF8" | ConvertFrom-Json
            $Global:FilePath = $File.FullName

            # Console Information
            Write-Host "• Configuration File Detected"

            # Exit Search
            break :Search

        }

    }

    # Unable to Load Configuration File
    if (!($FileData.PSObject.Properties)) {

        # Console Information
        Write-Host "• Configuration File Error" -ForegroundColor "Yellow"
        Start-Sleep -Mi 500

        # Console Seperation
        Write-Host ""

        # Exit Session
        Termination

    }

    # Unable to Locate Configuration File
    if (!(([System.IO.FileInfo]$FilePath).Exists)) {

        # Console Information
        Write-Host "• Configuration File Undetected" -ForegroundColor "Yellow"
        Start-Sleep -Mi 500

        # Console Seperation
        Write-Host ""

        # Exit Session
        Termination

    }

    # Success in Locating Configuration File
    if (([System.IO.FileInfo]$FilePath).Exists) {

        # Create Encryption List
        $Global:EncryptList = [System.Collections.Generic.List[Object]]::New()
        $FileData.Modifiable.Encrypt | ForEach-Object {
            $Element = $ExecutionContext.SessionState.Path.GetUnresolvedProviderPathFromPSPath($ExecutionContext.InvokeCommand.ExpandString($_))
            $EncryptList.Add($Element)
        }

        # Create File List
        $Global:FileList = [System.Collections.Generic.List[Object]]::New()
        [System.IO.Directory]::GetFiles($ProcessRoute) | ForEach-Object {
            $FileList.Add($_)
        }

    }

}

function Termination() {

    # Console Title
    $Host.UI.RawUI.WindowTitle = "Executable Locker (Termination)"

    # Console Information
    Write-Host "Executing Program Termination" -ForegroundColor "Red"
    Start-Sleep -Mi 250
    Write-Host "• Program Termination 00:00:03" -ForegroundColor "Red"
    Start-Sleep -Mi 1000
    Write-Host "• Program Termination 00:00:02" -ForegroundColor "Red"
    Start-Sleep -Mi 1000
    Write-Host "• Program Termination 00:00:01" -ForegroundColor "Red"
    Start-Sleep -Mi 1000
    Write-Host "• Program Termination 00:00:00" -ForegroundColor "Red"
    Start-Sleep -Mi 500

    # Terminate PowerShell
    Stop-Process -ID $PID

}

# Setup Password Module
function Password() {

    # Console Information
    Write-Host "• Loading Password Module"

    # Console Title
    $Host.UI.RawUI.WindowTitle = "Executable Locker (Password)"

    # Set Password File
    $File = $ExecutionContext.SessionState.Path.GetUnresolvedProviderPathFromPSPath($ExecutionContext.InvokeCommand.ExpandString($ProcessRoute + "\" + "Password"))

    # Load Assembly
    [System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms")  | Out-Null
    [System.Reflection.Assembly]::LoadWithPartialName("System.Drawing")  | Out-Null

    # Load Visual Styles
    # [Windows.Forms.Application]::EnableVisualStyles()

    function DetectPassword() {
        foreach ($Element in $FileList) {
            if ([System.IO.Path]::GetFileName($Element) -Match "^[A-F0-9]{8}-([A-F0-9]{4}-){3}[A-F0-9]{12}\.(zip)\.([0-9]){3}\.part") {
                return $True
                break
            } else {
                continue
            }
        }
        if ((([System.IO.FileInfo]$File).Exists) -OR ($FileData.Unmodifiable.Database -Match "^[A-F0-9]{8}-([A-F0-9]{4}-){3}[A-F0-9]{12}$")) {
            return $True
            break
        } elseif ((!(([System.IO.FileInfo]$File).Exists)) -OR (!($FileData.Unmodifiable.Database -Match "^[A-F0-9]{8}-([A-F0-9]{4}-){3}[A-F0-9]{12}$"))) {
            return $False
            break
        }
    }

    function CreateEnvironment() {

        # Exit Environment
        try {
            $Window.Close()
        } catch {
        }

        # Create Window
        $Global:Window = New-Object System.Windows.Forms.Form
        $Window.Size = New-Object System.Drawing.Size(300,130)
        $Window.StartPosition = "CenterScreen"
        $Window.Topmost = $False

        # Prevent Resize
        $Window.FormBorderStyle = "FixedDialog"
        $Window.MinimumSize = New-Object System.Drawing.Size(300,130)
        $Window.MaximumSize = New-Object System.Drawing.Size(300,130)

        # Message Dialog
        $Global:Message = New-Object System.Windows.Forms.Label
        $Message.Location = New-Object System.Drawing.Point(10,10)
        $Message.Size = New-Object System.Drawing.Size(280,20)

        # User Password Input Box
        $Global:Password = New-Object System.Windows.Forms.TextBox
        $Password.Location = New-Object System.Drawing.Point(10,30)
        $Password.Size = New-Object System.Drawing.Size(260,20)
        $Window.Controls.Add($Password)

        # Unlock and Submit Button
        $Global:Button = New-Object System.Windows.Forms.Button
        $Button.Location = New-Object System.Drawing.Point(10,60)
        $Button.Size = New-Object System.Drawing.Size(70,20)
        $Button.DialogResult = [System.Windows.Forms.DialogResult]::OK
        $Window.AcceptButton = $Button

        # Hide and Remove Password Checkbox
        $Global:Checkbox = New-Object System.Windows.Forms.CheckBox
        $Checkbox.Location = New-Object System.Drawing.Point(90,60)
        $Checkbox.Size = New-Object System.Drawing.Size(130,20)

        # Set Values for Existing Password Window
        if (DetectPassword) {

            # Change Title
            $Window.Text = "Password Request"

            # Message Dialog
            $Message.Text = "Please enter the password to continue."
            $Window.Controls.Add($Message)

            # Unlock Button
            $Button.Text = "Unlock"
            $Window.Controls.Add($Button)

            # Remove Password Checkbox
            $Checkbox.Text = "Remove Password"
            if ([String]::IsNullOrEmpty($Tickbox)) {
                $Checkbox.Checked = $False
            } else {
                $Checkbox.Checked = $Tickbox
            }
            $Window.Controls.Add($Checkbox)

        } elseif (!(DetectPassword)) {

            # Change Title
            $Window.Text = "Password Creation"

            # Message Dialog
            $Message.Text = "Please set a new password."
            $Window.Controls.Add($Message)

            # Confirmation Button
            $Button.Text = "Submit"
            $Window.Controls.Add($Button)

            # Hide Password Checkbox
            $Checkbox.Text = "Hide Password File"
            if ([String]::IsNullOrEmpty($Tickbox)) {
                $Checkbox.Checked = $True
            } else {
                $Checkbox.Checked = $Tickbox
            }
            $Window.Controls.Add($Checkbox)

        }

        # Load Icon from File
        $Icon = [System.Drawing.Icon]::ExtractAssociatedIcon("$env:SystemRoot\System32\WindowsPowershell\v1.0\powershell.exe")

        # Load Icon into Bytes
        $MemoryStream = New-Object System.IO.MemoryStream
        $Icon.Save($MemoryStream)
        $Bytes = $MemoryStream.ToArray()
        $MemoryStream.Flush()
        $MemoryStream.Dispose()

        # Load Icon into Interface
        $Stream = [System.IO.MemoryStream]::New($Bytes, 0, $Bytes.Length)
        $Window.Icon = [System.Drawing.Icon]::FromHandle(([System.Drawing.Bitmap]::New($Stream).GetHIcon()))

    }

    function CloseEnvironment() {

        Param (

            # Simple Parameter
            [Parameter(Mandatory=$False)]
            [Switch]$Force

            # Extended Parameter
            # [Parameter(Mandatory=$False, Position=0)]
            # [ValidatePattern("^\d*[a-zA-Z][a-zA-Z0-9]*$")]
            # [String]$Force

        )

        if ($Force) {

            # Terminate Password Window Environment [Limit to Single Instance Mode]
            Get-Process | Where-Object {($_.ProcessName -EQ "PowerShell") -AND ($_.MainWindowTitle -EQ $Window.Text)} | Stop-Process -Force

        }

        # Exit Password Window Environment
        try {
            $Pipeline.Input.Close()
            $Pipeline.Dispose()
            $Runspace.Dispose()
            $Runspace.Close()
        } catch {
        }

    }

    function LoadEnvironment() {

        # Create Password Window Environment [Synchronous]
        # $Result = $Window.ShowDialog()

        # Create Password Window Environment [Asynchronous]
        $Global:Runspace = [Management.Automation.Runspaces.RunspaceFactory]::CreateRunspace()
        $Runspace.Open()
        $Runspace.SessionStateProxy.SetVariable("Window", $Window)
        $Runspace.SessionStateProxy.SetVariable("Password", $Password)
        $Runspace.SessionStateProxy.SetVariable("Checkbox", $Checkbox)
        $Global:Pipeline = $Runspace.CreatePipeline({[Void]$Window.ShowDialog()})

        # Show Password Window
        $Pipeline.InvokeAsync()

    }

    function AwaitEnvironment() {

        # Awaiting Password Window to Launch
        while ($Window.Visible -NE $True) {

            # Console Pause in Loop
            Start-Sleep -Mi 500

            # Initalise Counter
            if ([String]::IsNullOrEmpty($AwaitCounter)) {
                AwaitCounter = 1
            } else {
                AwaitCounter = AwaitCounter + 1
            }

            # Execute if Waiting Long
            if ((AwaitCounter -GE 5) -AND ($AwaitFailure -NE $True)) {

                # Reset Counter
                AwaitCounter = 0

                if ([String]::IsNullOrEmpty($Announcement)) {

                    # Console Information
                    Write-Host "• Password Environment Error" -ForegroundColor "Yellow"

                    # Mark as Message Shown
                    $Announcement = $True

                }

                # Exit Window Forcefully
                CloseEnvironment -Force

                # Setup Window
                CreateEnvironment

                # Launch Window
                LoadEnvironment

                # Mark as Failed
                $AwaitFailure = $True

            }

        }

        # Move Password Window to Foreground
        $MainWindowHandle = Get-Process | Where-Object {$_.MainWindowTitle -EQ $Window.Text} | Select-Object -ExpandProperty "MainWindowHandle"
        Add-Type 'using System; using System.Runtime.InteropServices; public class APIFunction { [DllImport("user32.dll")] [return: MarshalAs(UnmanagedType.Bool)] public static extern bool SetForegroundWindow(IntPtr hWnd); }'
        [Void][APIFunction]::SetForegroundWindow($MainWindowHandle)

        # Activate in Event of Entered Password
        if (!([String]::IsNullOrWhitespace($Passcode))) {

            # Simulate Keypress
            Add-Type -AssemblyName "System.Windows.Forms"
            [System.Windows.Forms.SendKeys]::SendWait("$Passcode")

            # Highlight Text
            [System.Windows.Forms.SendKeys]::SendWait("^+{LEFT}")

        }

        # Awaiting Password Window to Close
        while (Get-Process | Where-Object {$_.MainWindowTitle -EQ $Window.Text}) {

            # Console Pause in Loop
            Start-Sleep -Mi 500

        }

    }

    # Enter Existing Password Section
    if (DetectPassword) {

        # Console Information
        Write-Host "• Loading Password Request"

        # Remove Password for Reactivation of Window Environment
        Remove-Variable -Name "Password" -Scope "Global" -ErrorAction "SilentlyContinue"

        # Remove Previous Password to Not Show on First Run
        Remove-Variable -Name "Passcode" -Scope "Global" -ErrorAction "SilentlyContinue"

        # Determine if Password File Exists
        if (([System.IO.FileInfo]$File).Exists) {

            # Decode Password from File
            $Decoded = [System.Text.Encoding]::Unicode.GetString([System.Convert]::FromBase64String([System.IO.File]::ReadAllText($File)))

            if ($True) {

                # Bypass Experimental Feature
                # • Remove the database string from password file.

                $Content = $Decoded -Replace "^\{[A-F0-9]{8}-([A-F0-9]{4}-){3}[A-F0-9]{12}\}"

            } else {

                # Work in Progress
                # • Attempt to prevent using a new generated password file.
                # • Save the database string to the password file.
                # • Required to update the password file on extraction and compression.

                # Able to Find Database in Password
                if ($Decoded -Match "^\{[A-F0-9]{8}-([A-F0-9]{4}-){3}[A-F0-9]{12}\}") {

                    # Database is Equivalent to Current Database
                    if ($Matches[0] -EQ $FileData.Unmodifiable.Database) {

                        # Set Password as Variable
                        $Content = $Decoded -Replace $Matches[0]

                    # Database is Different to Current Database
                    } elseif ($Matches[0] -NE $FileData.Unmodifiable.Database) {

                        # Console Information
                        Write-Host "• Password Load Mismatch" -ForegroundColor "Yellow"

                        # Console Seperation
                        Write-Host ""

                        # Exit Session
                        Termination

                    }

                # Unable to Find Database in Password
                } elseif ($Decoded -NotMatch "^\{[A-F0-9]{8}-([A-F0-9]{4}-){3}[A-F0-9]{12}\}") {

                    # Console Information
                    Write-Host "• Password Load Error" -ForegroundColor "Yellow"

                    # Console Seperation
                    Write-Host ""

                    # Exit Session
                    Termination

                }

            }

        }

        # Password File Tamperment Detection
        if ((!(([System.IO.FileInfo]$File).Exists)) -OR ((([System.IO.FileInfo]$File).Length) -EQ 0) -OR (!([System.IO.File]::GetAttributes($File).ToString().Contains("Encrypted"))) -OR ([String]::IsNullOrWhitespace($Content)) -OR ($Content -NotMatch "^[a-zA-Z0-9\ \'\-\[\]{}()+=_.,;@~#!£$€¥%^&×¤]+$") -OR ($Content -Match "[\r\n]+")) {

            # Prevent Proceeding
            $Content = [System.Web.Security.Membership]::GeneratePassword("128", "20")

        }

        # Loop if Password is Incorrect
        while ($Password.Text -NE $Content) {

            # Setup Window
            CreateEnvironment

            # Exit Window
            CloseEnvironment

            # Launch Window
            LoadEnvironment

            # Pause Terminal
            AwaitEnvironment

            # Set Password and Checkbox to Previous State
            $Global:Passcode = $Password.Text
            $Global:Tickbox = $Checkbox.Checked

            # Confirmation Button was Pressed
            if (($Window.DialogResult -EQ [System.Windows.Forms.DialogResult]::OK) -AND (($Password.Text).Length -GT 0) -AND ($Password.Text -Match "^[a-zA-Z0-9\ \'\-\[\]{}()+=_.,;@~#!£$€¥%^&×¤]+$")) {

                # Incorrect Password was Entered
                if (($Password.Text -NE $Content) -AND ([String]::IsNullOrEmpty($Notification))) {

                    # Console Information
                    Write-Host "• Password Request Failure" -ForegroundColor "Yellow"

                    # Mark as Message Shown
                    $Notification = $True

                }

                # Correct Password was Entered
                if ($Password.Text -EQ $Content) {

                    # Console Information
                    Write-Host "• Password Request Success"

                    # Checkbox is Selected
                    if ($Checkbox.Checked -EQ $True) {

                        # Mark as Deleted
                        $Global:DeleteMode = $True

                        # Delete Password File
                        Remove-Item -Path $File -Force

                        # Console Information
                        Write-Host "• Password File Deleted"

                    }

                    # Update in Event of Non-Existing Database String
                    if (!($FileData.Unmodifiable.Database -Match "^[A-F0-9]{8}-([A-F0-9]{4}-){3}[A-F0-9]{12}$")) {

                        # Update JavaScript Object Notation File
                        [System.IO.File]::ReadAllText($FilePath) | ForEach-Object {$_ -Replace @("""Database"": ""(.*?)""", """Database"": ""X0000000-X000-X000-X000-X00000000000""")} | Set-Content ($FilePath)

                        # Remove Trailing Line from File
                        [System.IO.File]::WriteAllText($FilePath, ([System.IO.File]::ReadAllText($FilePath)).Trim())

                    }

                    # Search for Part File
                    foreach ($Element in $FileList) {

                        # Part File was Detected
                        if ([System.IO.Path]::GetFileName($Element) -Match "^[A-F0-9]{8}-([A-F0-9]{4}-){3}[A-F0-9]{12}\.(zip)\.([0-9]){3}\.part") {

                            # Grab the File Name in Part File Syntax
                            $PartGUID = (([System.IO.Path]::GetFileName($Element)).Substring(0,([System.IO.Path]::GetFileName($Element)).IndexOf(".zip.")))

                            # Update JavaScript Object Notation File
                            [System.IO.File]::ReadAllText($FilePath) | ForEach-Object {$_ -Replace @("""Database"": ""(.*?)""", """Database"": ""$PartGUID""")} | Set-Content ($FilePath)

                            # Remove Trailing Line from File
                            [System.IO.File]::WriteAllText($FilePath, ([System.IO.File]::ReadAllText($FilePath)).Trim())

                            break

                        } else {

                            continue

                        }

                    }

                }

            # Blank or Non-Alphanumeric Password was Entered
            } elseif (($Window.DialogResult -EQ [System.Windows.Forms.DialogResult]::OK) -AND ((($Password.Text).Length -EQ 0) -OR ($Password.Text -NotMatch "^[a-zA-Z0-9\ \'\-\[\]{}()+=_.,;@~#!£$€¥%^&×¤]+$")) -AND ([String]::IsNullOrEmpty($ErrorMessage))) {

                # Console Information
                Write-Host "• Password Syntax Error" -ForegroundColor "Yellow"

                # Mark as Message Shown
                $ErrorMessage = $True

            # Cancellation Button was Pressed
            } elseif ($Window.DialogResult -EQ [System.Windows.Forms.DialogResult]::Cancel) {

                # Console Information
                Write-Host "• Password Request Cancelled" -ForegroundColor "Yellow"
                Start-Sleep -Mi 500

                # Console Seperation
                Write-Host ""

                # Exit Session
                Termination

            }

        }

    # Set Password Section
    } elseif (!(DetectPassword)) {

        # Console Information
        Write-Host "• Loading Password Creation"

        # Remove Password for Reactivation of Window Environment
        Remove-Variable -Name "Password" -Scope "Global" -ErrorAction "SilentlyContinue"

        # Remove Previous Password to Not Show on First Run
        Remove-Variable -Name "Passcode" -Scope "Global" -ErrorAction "SilentlyContinue"

        # Loop if Password is Undetectable or File Doesn't Exist
        while ((($Password.Text).Length -EQ 0) -OR (!([System.IO.FileInfo]$File).Exists)) {

            # Setup Window
            CreateEnvironment

            # Exit Window
            CloseEnvironment

            # Launch Window
            LoadEnvironment

            # Pause Terminal
            AwaitEnvironment

            # Set Checkbox to Previous State
            $Global:Passcode = $Password.Text
            $Global:Tickbox = $Checkbox.Checked

            # Confirmation Button was Pressed
            if (($Window.DialogResult -EQ [System.Windows.Forms.DialogResult]::OK) -AND (($Password.Text).Length -GT 0) -AND ($Password.Text -Match "^[a-zA-Z0-9\ \'\-\[\]{}()+=_.,;@~#!£$€¥%^&×¤]+$")) {

                # Save Password to File
                $Content = "{" + $ProcessGUID + "}" + $Password.Text
                $Encoded = [Convert]::ToBase64String([Text.Encoding]::Unicode.GetBytes($Content))
                [System.IO.File]::WriteAllLines($File, $Encoded)

                # Remove Last Line of File
                [System.IO.File]::WriteAllText($File, ([System.IO.File]::ReadAllText($File)).Trim())

                # Encrypt Password File
                [System.IO.File]::Encrypt($File)

                # Close Notification of Encrypting File System
                # • Generated on first instance on encrypting a file.
                Get-Process | Where-Object {($_.ProcessName -EQ "efsui.exe") -OR ($_.Description -EQ "EFS UI Application")} | Stop-Process -Force

                # Console Information
                Write-Host "• Password Creation Success"

                # Update in Event of Non-Existing Database String
                if (!($FileData.Unmodifiable.Database -Match "^[A-F0-9]{8}-([A-F0-9]{4}-){3}[A-F0-9]{12}$")) {

                    # Update JavaScript Object Notation File
                    [System.IO.File]::ReadAllText($FilePath) | ForEach-Object {$_ -Replace @("""Database"": ""(.*?)""", """Database"": ""X0000000-X000-X000-X000-X00000000000""")} | Set-Content ($FilePath)

                    # Remove Trailing Line from File
                    [System.IO.File]::WriteAllText($FilePath, ([System.IO.File]::ReadAllText($FilePath)).Trim())

                }

                # Search for Part File
                foreach ($Element in $FileList) {

                    # Part File was Detected
                    if ([System.IO.Path]::GetFileName($Element) -Match "^[A-F0-9]{8}-([A-F0-9]{4}-){3}[A-F0-9]{12}\.(zip)\.([0-9]){3}\.part") {

                        # Grab the File Name in Part File Syntax
                        $PartGUID = (([System.IO.Path]::GetFileName($Element)).Substring(0,([System.IO.Path]::GetFileName($Element)).IndexOf(".zip.")))

                        # Update JavaScript Object Notation File
                        [System.IO.File]::ReadAllText($FilePath) | ForEach-Object {$_ -Replace @("""Database"": ""(.*?)""", """Database"": ""$PartGUID""")} | Set-Content ($FilePath)

                        # Remove Trailing Line from File
                        [System.IO.File]::WriteAllText($FilePath, ([System.IO.File]::ReadAllText($FilePath)).Trim())

                        break

                    } else {

                        continue

                    }

                }

                # Checkbox is Selected
                if ($Checkbox.Checked -EQ $True) {

                    # Hide Password File
                    Set-ItemProperty -Path $File -Name "Attributes" -Value "Hidden, System"

                    # Console Information
                    Write-Host "• Password Attributes Modified"

                }

            # Blank or Non-Alphanumeric Password was Entered
            } elseif (($Window.DialogResult -EQ [System.Windows.Forms.DialogResult]::OK) -AND ((($Password.Text).Length -EQ 0) -OR ($Password.Text -NotMatch "^[a-zA-Z0-9\ \'\-\[\]{}()+=_.,;@~#!£$€¥%^&×¤]+$")) -AND ([String]::IsNullOrEmpty($ErrorMessage))) {

                # Console Information
                Write-Host "• Password Syntax Error" -ForegroundColor "Yellow"

                # Mark as Message Shown
                $ErrorMessage = $True

            # Cancellation Button was Pressed
            } elseif ($Window.DialogResult -EQ [System.Windows.Forms.DialogResult]::Cancel) {

                # Console Information
                Write-Host "• Password Creation Cancelled" -ForegroundColor "Yellow"
                Start-Sleep -Mi 500

                # Console Seperation
                Write-Host ""

                # Exit Session
                Termination

            }

        }

    }

}

# Setup Executable Module
function Executable() {

    # Console Information
    Write-Host "• Loading Executable Module"

    # Define Executable File
    $File = $ExecutionContext.SessionState.Path.GetUnresolvedProviderPathFromPSPath($ExecutionContext.InvokeCommand.ExpandString($FileData.Modifiable.Executable))

    # Existing Executable File
    if (([System.IO.FileInfo]$File).Exists) {

        # Define Window Title
        $WindowTitle = "Executable Locker ($([System.IO.Path]::GetFileName($File)))"

        # Console Title
        $Host.UI.RawUI.WindowTitle = $WindowTitle

    } elseif ($FileData.Unmodifiable.Database -Match "^[A-F0-9]{8}-([A-F0-9]{4}-){3}[A-F0-9]{12}$") {

        # Define Window Title
        $WindowTitle = "Executable Locker ($($FileData.Unmodifiable.Database))"

        # Console Title
        $Host.UI.RawUI.WindowTitle = $WindowTitle

    } else {

        # Define Window Title
        $WindowTitle = "Executable Locker ($ProcessGUID)"

        # Console Title
        $Host.UI.RawUI.WindowTitle = $WindowTitle

    }

    # Detect if Executable Exists
    if (([System.IO.FileInfo]$File).Exists) {

        # Console Information
        Write-Host "• Executable File Detected"

        # Define Executable Process
        $Executable = Get-Process | Where-Object {$_.Path -EQ $File}
        $ProcessID = $Executable | Select-Object -ExpandProperty "ID"

        # Ensure Executable isn't Already Running
        if ([String]::IsNullOrEmpty($Executable)) {

            # Console Information
            Write-Host "• Executable Process Launching"

            # Create Timeout Stopwatch
            $Stopwatch = [System.Diagnostics.Stopwatch]::StartNew()

            # Awaiting Program to Launch
            while ((([System.IO.FileInfo]$File).Exists) -AND (!(Get-Process | Where-Object {$_.ID -EQ $ProcessID})) -AND (!($Stopwatch.ElapsedMilliseconds -GE 30000))) {

                # Launch Executable
                $Executable = [System.Diagnostics.Process]::Start($File)
                $ProcessID = $Executable | Select-Object -ExpandProperty "ID"

                # Console Pause in Loop
                Start-Sleep -Mi 500

            }

        # Executable is Already Running
        } elseif (!([String]::IsNullOrEmpty($Executable))) {

            # Console Information
            Write-Host "• Executable Process Detected"

        }

        # Save Window State
        # • [System.Diagnostics.Process]::GetCurrentProcess().MainWindowHandle
        $PowerShell = (Get-Process | Where-Object {$_.MainWindowTitle -EQ $WindowTitle}).MainWindowHandle

        # Minimise Process
        # Add-Type -MemberDefinition '[DllImport("user32.dll")] public static extern bool ShowWindowAsync(IntPtr hWnd, int nCmdShow);' -Name "NativeMethods" -NameSpace "Win32"
        # [Win32.NativeMethods]::ShowWindowAsync($PowerShell, 2) | Out-Null

        # Hide Process
        Add-Type -Member '[DllImport("user32.dll")] public static extern bool ShowWindow(int handle, int state);' -Name "Win" -Namespace "Native"
        [Native.Win]::ShowWindow($PowerShell, 0) | Out-Null

        # Console Information
        Write-Host "• Executable Awaiting Termination"

        # Awaiting Program to Close
        while ((([System.IO.FileInfo]$File).Exists) -AND (Get-Process | Where-Object {$_.ID -EQ $ProcessID})) {

            # Console Pause in Loop
            Start-Sleep -Mi 500

        }

        # Console Information
        Write-Host "• Executable Termination Detected"

        # Maximise Process
        # Add-Type -MemberDefinition '[DllImport("user32.dll")] public static extern bool ShowWindowAsync(IntPtr hWnd, int nCmdShow);' -Name "NativeMethods" -NameSpace "Win32"
        # [Win32.NativeMethods]::ShowWindowAsync($PowerShell, 4) | Out-Null

        # Show Process
        Add-Type -Member '[DllImport("user32.dll")] public static extern bool ShowWindow(int handle, int state);' -Name "Win" -Namespace "Native"
        [Native.Win]::ShowWindow($PowerShell, 1) | Out-Null

    } elseif (!(([System.IO.FileInfo]$File).Exists)) {

        # Console Information
        Write-Host "• Executable File Undetected"

    }

}

# Setup Compression Module
function Compression() {

    # Console Information
    Write-Host "• Loading Compression Module"

    # Console Title
    $Host.UI.RawUI.WindowTitle = "Executable Locker (Compression)"

    # Define Executable File
    $Executable = Get-Process | Where-Object {$_.Path -EQ $ExecutionContext.SessionState.Path.GetUnresolvedProviderPathFromPSPath($ExecutionContext.InvokeCommand.ExpandString($FileData.Modifiable.Executable))}

    # Running Executable Detection
    if (!([String]::IsNullOrEmpty($Executable))) {

        # Close Program
        $Executable | Stop-Process -Force
        Start-Sleep -Mi 500

        # Console Information
        Write-Host "• Terminating Running Executable"

    }

    # Set Compression Level
    if ((!([String]::IsNullOrEmpty($FileData.Modifiable.Compression))) -OR ($FileData.Modifiable.Compression -EQ "NoCompression") -OR ($FileData.Modifiable.Compression -EQ "2") -OR ($FileData.Modifiable.Compression -EQ "None") -OR ($FileData.Modifiable.Compression -EQ "No") -OR ($FileData.Modifiable.Compression -EQ "Disabled") -OR ($FileData.Modifiable.Compression -EQ "Disable") -OR ($FileData.Modifiable.Compression -EQ "False")) {
        $Compression = [System.IO.Compression.CompressionLevel]::NoCompression
    } elseif (($FileData.Modifiable.Compression -EQ "Fastest") -OR ($FileData.Modifiable.Compression -EQ "1") -OR ($FileData.Modifiable.Compression -EQ "Yes") -OR ($FileData.Modifiable.Compression -EQ "Enabled") -OR ($FileData.Modifiable.Compression -EQ "Enable") -OR ($FileData.Modifiable.Compression -EQ "True")) {
        $Compression = [System.IO.Compression.CompressionLevel]::Fastest
    } elseif (($FileData.Modifiable.Compression -EQ "Optimal") -OR ($FileData.Modifiable.Compression -EQ "0")) {
        $Compression = [System.IO.Compression.CompressionLevel]::Optimal
    } elseif ([String]::IsNullOrEmpty($Compression)) {
        $Compression = [System.IO.Compression.CompressionLevel]::NoCompression
    }

    # Run Through User-Defined List
    $EncryptList | ForEach-Object {

        # Check if User-Defined Files Exists
        if ((([System.IO.FileInfo]$_).Exists) -OR (([System.IO.DirectoryInfo]$_).Exists)) {

            # Check if Archive Exists
            if (!(([System.IO.FileInfo]($ProcessRoute + "\" + $ProcessGUID + ".zip")).Exists)) {

                # Execute in Event of Non-Existing Encrpyiton Folder
                if (!(([System.IO.DirectoryInfo]($ProcessRoute + "\" + $ProcessGUID)).Exists)) {

                    # Create Encrpyiton Folder
                    [System.IO.Directory]::CreateDirectory($ProcessRoute + "\" + $ProcessGUID) | Out-Null

                }

                # Execute in Event of Existing Encrpyiton Folder
                if (([System.IO.DirectoryInfo]($ProcessRoute + "\" + $ProcessGUID)).Exists) {

                    # Move User-Defined List to Encrpyiton Folder
                    Move-Item -Path $_ -Destination ($ProcessRoute + "\" + $ProcessGUID) -Force

                }

            # Archive can be Updated
            } elseif (([System.IO.FileInfo]($ProcessRoute + "\" + $ProcessGUID + ".zip")).Exists) {

                # Target is a File
                if (([System.IO.FileInfo]$_).Exists) {

                    # Perform Archive Update
                    [Reflection.Assembly]::LoadWithPartialName("System.IO.Compression.FileSystem") | Out-Null
                    [System.IO.Compression.ZipArchive]$Archive = [System.IO.Compression.ZipFile]::Open(($ProcessRoute + "\" + $ProcessGUID + ".zip"), ([System.IO.Compression.ZipArchiveMode]::Update))
                    [System.IO.Compression.ZipFileExtensions]::CreateEntryFromFile($Archive, $_, (Split-Path $_ -Leaf), $Compression) | Out-Null
                    $Archive.Dispose()

                    # Remove File
                    Remove-Item -Path $_ -Force -WhatIf

                # Target is a Folder
                } elseif (([System.IO.DirectoryInfo]$_).Exists) {

                    # Ensure Folder isn't Empty
                    if ((Get-ChildItem $_ | Measure-Object | Select-Object -ExpandProperty "Count") -NE 0) {

                        # Hide Progress Bar
                        $ProgressPreference = "SilentlyContinue"

                        # Perform Archive Update
                        [Reflection.Assembly]::LoadWithPartialName("System.IO.Compression.FileSystem") | Out-Null
                        Compress-Archive -Path $_ -DestinationPath ($ProcessRoute + "\" + $ProcessGUID + ".zip") -CompressionLevel $Compression -Update

                        # Prevent Deleting Route Directory
                        if (($_) -NE ($ProcessRoute + "\")) {

                            # Remove Folder
                            Remove-Item -Path $_ -Force -Recurse

                        }

                    }

                }

            }

        }

    }

    # Execute in Event of Non-Existing Encrpyiton Archive
    if ((!(([System.IO.FileInfo]($ProcessRoute + "\" + $ProcessGUID + ".zip")).Exists)) -AND (([System.IO.DirectoryInfo]($ProcessRoute + "\" + $ProcessGUID)).Exists)) {

        # Create Archive of Encrpyiton Folder
        [Reflection.Assembly]::LoadWithPartialName("System.IO.Compression.FileSystem") | Out-Null
        [System.IO.Compression.ZipFile]::CreateFromDirectory(($ProcessRoute + "\" + $ProcessGUID), ($ProcessRoute + "\" + $ProcessGUID + ".zip"), ([System.IO.Compression.CompressionLevel]::NoCompression), $False)

        # Update JavaScript Object Notation File
        [System.IO.File]::ReadAllText($FilePath) | ForEach-Object {$_ -Replace @("""Database"": ""(.*?)""", """Database"": ""$ProcessGUID""")} | Set-Content ($FilePath)

        # Remove Trailing Line from File
        [System.IO.File]::WriteAllText($FilePath, ([System.IO.File]::ReadAllText($FilePath)).Trim())

        # Prevent Deleting Route Directory
        if (($ProcessRoute + "\" + $ProcessGUID) -NE ($ProcessRoute + "\")) {

            # Delete Encrpyiton Folder
            Remove-Item -Path ($ProcessRoute + "\" + $ProcessGUID) -Force -Recurse

        }

    }

}

# Setup Extraction Module
function Extraction() {

    # Console Information
    Write-Host "• Loading Extraction Module"

    # Console Title
    $Host.UI.RawUI.WindowTitle = "Executable Locker (Extraction)"

    # Grab Updated Configuration File
    $FileData = Get-Content -Path $FilePath -Raw -Encoding "UTF8" | ConvertFrom-Json

    # Define Executable File
    $Executable = Get-Process | Where-Object {$_.Path -EQ $ExecutionContext.SessionState.Path.GetUnresolvedProviderPathFromPSPath($ExecutionContext.InvokeCommand.ExpandString($FileData.Modifiable.Executable))}

    # Running Executable Detection
    if (!([String]::IsNullOrEmpty($Executable))) {

        # Close Program
        $Executable | Stop-Process -Force
        Start-Sleep -Mi 500

        # Console Information
        Write-Host "• Terminating Running Executable"

    }

    # Run Through User-Defined List
    $EncryptList | ForEach-Object {

        # Execute Regardless of File Existance
        if (((([System.IO.FileInfo]$_).Exists) -OR (([System.IO.DirectoryInfo]$_).Exists)) -OR (!((([System.IO.FileInfo]$_).Exists)) -OR (!(([System.IO.DirectoryInfo]$_).Exists)))) {

            # Execute in Event of Existing Archive
            if (([System.IO.FileInfo]($ProcessRoute + "\" + $FileData.Unmodifiable.Database + ".zip")).Exists) {

                # Extract Encrpyiton Folder
                [Reflection.Assembly]::LoadWithPartialName("System.IO.Compression.FileSystem") | Out-Null
                [System.IO.Compression.ZipFile]::ExtractToDirectory(($ProcessRoute + "\" + $FileData.Unmodifiable.Database + ".zip"), ($ProcessRoute + "\" + $FileData.Unmodifiable.Database))

                # Update JavaScript Object Notation File
                [System.IO.File]::ReadAllText($FilePath) | ForEach-Object {$_ -Replace @("""Database"": ""(.*?)""", """Database"": ""X0000000-X000-X000-X000-X00000000000""")} | Set-Content ($FilePath)

                # Remove Trailing Line from File
                [System.IO.File]::WriteAllText($FilePath, ([System.IO.File]::ReadAllText($FilePath)).Trim())

                # Delete Archived Folder
                Remove-Item -Path ($ProcessRoute + "\" + $FileData.Unmodifiable.Database + ".zip") -Force

            }

            # Execute in Event of Existing Encrpyiton Folder
            if (([System.IO.DirectoryInfo]($ProcessRoute + "\" + $FileData.Unmodifiable.Database)).Exists) {

                # Element Moving from Encryption Folder Must Exist
                if ((([System.IO.FileInfo]($ProcessRoute + "\" + $FileData.Unmodifiable.Database + "\" + [System.IO.Path]::GetFileName($_))).Exists) -OR (([System.IO.DirectoryInfo]($ProcessRoute + "\" + $FileData.Unmodifiable.Database + "\" + [System.IO.Path]::GetFileName($_))).Exists)) {

                    # Move Encrpyiton List from Encrpyiton Folder
                    Move-Item -Path ($ProcessRoute + "\" + $FileData.Unmodifiable.Database + "\" + [System.IO.Path]::GetFileName($_)) -Destination $_ -Force

                }

            }

        }

    }

    # Execute in Event of Existing Encrpyiton Folder
    if (([System.IO.DirectoryInfo]($ProcessRoute + "\" + $FileData.Unmodifiable.Database)).Exists) {

        # Prevent Deleting Route Directory
        if (($ProcessRoute + "\" + $FileData.Unmodifiable.Database) -NE ($ProcessRoute + "\")) {

            # Delete Encrpyiton Folder
            Remove-Item -Path ($ProcessRoute + "\" + $FileData.Unmodifiable.Database) -Force -Recurse

        }

    }

}

# Setup Encryption Module
function Encryption() {

    # Console Information
    Write-Host "• Loading Encryption Module"

    # Console Title
    $Host.UI.RawUI.WindowTitle = "Executable Locker (Encryption)"

    # Define Variables
    $TargetPath = $ExecutionContext.SessionState.Path.GetUnresolvedProviderPathFromPSPath($ProcessRoute + "\" + $ProcessGUID + ".zip")
    $TargetFile = [System.IO.Path]::GetFileName($TargetPath)
    $TargetSize = ([System.IO.FileInfo]$TargetPath).Length
    $TargetParent = [System.IO.Path]::GetDirectoryName($TargetPath)

    # Define More Variables
    $SourceFileStream = [IO.File]::OpenRead($TargetPath)
    $TotalBytesRemaining = $TargetSize
    $PartFileLoaded = 1
    $BytesLoaded = 0
    $PaddingLength = 3

    # Define Even More Variables
    $PartSizeBytes = [Math]::Ceiling($FileData.Modifiable.Bytes)
    $PartFileTotal = [Math]::Ceiling($TargetSize / $PartSizeBytes)
    $Buffer = New-Object -TypeName Byte[] -ArgumentList $PartSizeBytes

    # Overall Bytes Remaining Checker
    while ($TotalBytesRemaining -GT 0) {

        # Define Part File Path and Size
        $PartFilePath = [String]::Format("$TargetParent\$TargetFile.{0:D$PaddingLength}.part", $PartFileLoaded)
        $OutputStream = [System.IO.File]::Open($PartFilePath, "Create")
        $CurrentPartFileSize = $PartSizeBytes

        # Part Bytes Remaining Checker
        while ($CurrentPartFileSize -GT 0) {

            # Define Bytes Remaining to Read
            $BytesRemaining = $Buffer.Length

            # Parts Left Exceed Buffer Size Detection
            if ($CurrentPartFileSize -LT $BytesRemaining) {
                $BytesRemaining = $CurrentPartFileSize
            }

            # Bytes Left to Read Tracker
            $CurrentPartFileSize = $CurrentPartFileSize - $BytesRemaining

            # Read Buffer Content
            $BytesLoaded = $SourceFileStream.Read($Buffer, 0, $BytesRemaining)

            # Write Buffer Content
            $OutputStream.Write($Buffer, 0, $BytesLoaded)

        }

        # Exit Output Stream
        $OutputStream.Flush()
        $OutputStream.Close()

        # Encrypt Part File
        [System.IO.File]::Encrypt($PartFilePath)

        # Update Variables
        $TotalBytesRemaining = $TotalBytesRemaining - $BytesLoaded
        $PartFileLoaded = $PartFileLoaded + 1

    }

    # Exit Source Stream
    $SourceFileStream.Flush()
    $SourceFileStream.Close()

    # Remove Compressed Part File
    Remove-Item -Path $TargetPath -Force

}

# Setup Decryption Module
function Decryption() {

    # Console Information
    Write-Host "• Loading Decryption Module"

    # Console Title
    $Host.UI.RawUI.WindowTitle = "Executable Locker (Decryption)"

    # Grab Updated Configuration File
    $FileData = Get-Content -Path $FilePath -Raw -Encoding "UTF8" | ConvertFrom-Json

    # Define Variables
    $TargetPath = $ExecutionContext.SessionState.Path.GetUnresolvedProviderPathFromPSPath($ProcessRoute + "\" + $FileData.Unmodifiable.Database + ".zip")
    $TargetFile = [System.IO.Path]::GetFileName($TargetPath) -Replace "\.\d+$"
    $TargetParent = [System.IO.Path]::GetDirectoryName($TargetPath)
    $BufferSize = 4194304
    $PaddingLength = 3

    # Generate Part List
    $PartList = [System.Collections.ArrayList]::New()
    Get-ChildItem $TargetParent | Where-Object { ($_.Name -Match "$TargetFile\.(\d{$PaddingLength})\.part") } | Sort-Object -Property "Name" | ForEach-Object {

        # Create Part Object
        $PartObject = [PSCustomObject]@{
            "Name" = $_.Name
            "Path" = $_.FullName
            "Bytes" = $_.Length
        }

        # Expand Part List with Object
        [Void]$PartList.Add($PartObject)

    }

    # Set Source Path and Output Stream
    $SourcePath = [String]::Format("$TargetParent\$TargetFile")
    $OutputStream = [System.IO.File]::Open($SourcePath, "Create")

    # Run Through Part File List
    $PartList | ForEach-Object {

        # Decrypt Part Files
        [System.IO.File]::Decrypt($_.Path)

        # Merge Part Files
        $Reader = [IO.File]::OpenRead($_.Path)
        $Reader.CopyTo($OutputStream, $BufferSize)
        $Reader.Close()

        # Remove Part File
        Remove-Item -Path $_.Path -Force

    }

    # Exit Output Stream
    $OutputStream.Flush()
    $OutputStream.Close()

}


# Setup Destruction Module
function Destruction() {

    # Console Information
    Write-Host "• Loading Destruction Module"

    # Console Title
    $Host.UI.RawUI.WindowTitle = "Executable Locker (Destruction)"

    # Success in Locating Configuration File and Deletion List
    if ((([System.IO.FileInfo]$FilePath).Exists) -AND ([String]::IsNullOrEmpty($FileData.Modifiable.Delete))) {

        # Create Deletion List
        $FileData.Modifiable.Delete | ForEach-Object {

            $Element = $ExecutionContext.SessionState.Path.GetUnresolvedProviderPathFromPSPath($ExecutionContext.InvokeCommand.ExpandString($_))

            # Work in Progress
            # • Attempt to prevent deleting files within encryption list.
            # • Define deletion method permanent or recycle bin.

            # Prevent Deletion of Windows Files and Encrypted Data
            if (($Element -NotMatch "$env:HomeDrive\Windows") -AND (!($EncryptList.Contains($Element)))) {

                # Deletion Mode for Directoties
                if (([System.IO.DirectoryInfo]$Element).Exists) {

                    # Remove Subdirectoties and Files
                    Get-ChildItem -Path $Element -Recurse | ForEach-Object {
                        Remove-Item -Path $_.FullName -Force
                    }

                    # Remove Selected Directory
                    Get-Item -Path $Element | ForEach-Object {
                        Remove-Item -Path $_.FullName -Force
                    }

                }

                # Deletion Mode for Files
                if (([System.IO.FileInfo]$Element).Exists) {

                    # Remove Selected File
                    Get-Item -Path $Element | ForEach-Object {
                        Remove-Item -Path $_.FullName -Force
                    }

                }

            }

        }

    }

}

# Setup Finalisation Module
function Finalisation() {

    # Console Information
    Write-Host "• Loading Finalisation Module"

    # Console Title
    $Host.UI.RawUI.WindowTitle = "Executable Locker (Finalisation)"

    # Setup Message Function
    if (!($FileData.Unmodifiable.Database -Match "^[A-Z0-9]{8}-([A-Z0-9]{4}-){3}[A-Z0-9]{12}$")) {

        # Console Information
        Write-Host "• Program Execution Success" -ForegroundColor "Green"
        Start-Sleep -Mi 500

        # Console Seperation
        Write-Host ""

        # Console Information
        Write-Host "Warning Notification" -ForegroundColor "Yellow"
        Write-Host "• Please edit the configuration file before launching again." -ForegroundColor "Yellow"
        Write-Host "• Select files and folders to secure within the encrypt list." -ForegroundColor "Yellow"
        Write-Host "• Press [Enter] to terminate session." -ForegroundColor "Yellow"

        # Awaiting Any Key Press
        # $Host.UI.RawUI.ReadKey("NoEcho,IncludeKeyDown") | Out-Null
        # $Host.UI.RawUI.FlushInputBuffer()

        # Awaiting Enter Key Press
        while ($Enter -NE $True) {
            if ([System.Console]::KeyAvailable) {
                switch ([System.Console]::ReadKey().Key) {
                    "ENTER" {
                        $Enter = $True
                    }
                    { $True } {
                        try {
                            [Microsoft.PowerShell.PSConsoleReadLine]::SetCursorPosition(0)
                            [Microsoft.PowerShell.PSConsoleReadLine]::DeleteEndOfBuffer()
                        } catch {
                        }
                        Start-Sleep -Mi 100
                    }
                }
            }
        }

        # Terminate PowerShell
        Stop-Process -ID $PID

    } else {

        # Console Information
        Write-Host "• Program Execution Success" -ForegroundColor "Green"
        Start-Sleep -Mi 500

        # Console Seperation
        Write-Host ""

        # Exit Session
        Termination

    }

}

if ([String]::IsNullOrEmpty($FileData)) {

    Initalisation

}

# Run Normal Execution Mode
if ($FileData.Unmodifiable.Database -Match "^[A-F0-9]{8}-([A-F0-9]{4}-){3}[A-F0-9]{12}$") {

    Password
    if ([String]::IsNullOrEmpty($DeleteMode)) {
        Decryption
        Extraction
        Executable
        Compression
        Encryption
        Destruction
        Finalisation
    } elseif ($DeleteMode -EQ $True) {
        Decryption
        Extraction
        Destruction
        Finalisation
    }

# Run Crashed and Initalised Execution Mode
} elseif ($FileData.Unmodifiable.Database -Match "^[A-Z0-9]{8}-([A-Z0-9]{4}-){3}[A-Z0-9]{12}$") {

    Password
    if ([String]::IsNullOrEmpty($DeleteMode)) {
        Executable
        Compression
        Encryption
        Destruction
        Finalisation
    } elseif ($DeleteMode -EQ $True) {
        Decryption
        Extraction
        Destruction
        Finalisation
    }

# Run First-Run Execution Mode
} else {

    Password
    Finalisation

}